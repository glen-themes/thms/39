/*-----------------------------------------

PLEASE DO NOT STEAL.

Credits & Resources used can be found at:
glencredits.tumblr.com/clotho

-----------------------------------------*/

let currentURL = window.location.href.replace(/[?#].*|\/$/,"");
let currentPath = window.location.pathname.replace(/\/$/,"");
document.documentElement.setAttribute("current-path",currentPath);

const loadImage = (url) => new Promise((resolve, reject) => {
  const img = new Image();
  img.addEventListener('load', () => resolve(img));
  img.addEventListener('error', (err) => reject(err));
  img.src = url;
});

const screenDims = () => {
  let vpw = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0);
  let vph = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0);
  setRoot("--100vw",`${vpw}px`);
  setRoot("--100vh",`${vph}px`);
  setRoot("--1vw",`${vpw*0.01}px`);
  setRoot("--1vh",`${vph*0.01}px`);
  
  
  let header = document.querySelector("header");
  
  if(header){
    let headFlex = header.querySelector(".head-flex");
    let headLeft = headFlex?.querySelector(".head-left");
    let headRight = headFlex?.querySelector(".avi-cont");
    if(headFlex && headLeft && headRight){
      if(headRight.offsetWidth > vpw*0.25){
        headFlex.classList.add("mini");
      } else {
        headFlex.classList.remove("mini")
      }
      
      let nameText = headLeft.querySelector(".subt-cont")
      if(nameText){
        if(nameText.offsetWidth > vpw * 0.4){
          headLeft.classList.add("mini")
        } else {
          headLeft.classList.remove("mini")
        }
      }
    }
  }
}

const custard = () => {
  if(currentPath == "/customize_preview_receiver.html"){
    let _custard = document.createElement("div");
    _custard.classList.add("custard");
    
    let btn = document.createElement("a");
    btn.href = "https://dub.sh/clotho-guide";
    btn.target = "_blank";
    
    let icon1 = document.createElement("i");
    icon1.classList.add("hugeicons");
    icon1.setAttribute("icon-name","hand-pointing-right-03");
    
    let icon2 = document.createElement("i");
    icon2.classList.add("hugeicons");
    icon2.setAttribute("icon-name","link-square-02");
    
    let span = document.createElement("span");
    span.textContent = "Read the guide"
    
    document.body.append(_custard);
    _custard.append(btn);
    btn.append(icon1);
    btn.append(icon2);
    btn.append(span);
  }
}

/*------- MASK SVGS -------*/
// play icon
// flaticon.com/free-icon/play_727245
let xhkot = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns:svgjs='http://svgjs.com/svgjs' width='512' height='512' x='0' y='0' viewBox='0 0 320.001 320.001' style='enable-background:new 0 0 512 512' xml:space='preserve' class=''><g><path d='m295.84 146.049-256-144a16.026 16.026 0 0 0-15.904.128A15.986 15.986 0 0 0 16 16.001v288a15.986 15.986 0 0 0 7.936 13.824A16.144 16.144 0 0 0 32 320.001c2.688 0 5.408-.672 7.84-2.048l256-144c5.024-2.848 8.16-8.16 8.16-13.952s-3.136-11.104-8.16-13.952z' fill='black' data-original='black' class=''></path></g></svg>";

document.documentElement.style.setProperty("--Audio-Post-Play-Btn-SVG", `url("${xhkot}")`);

// pause icon
// flaticon.com/free-icon/pause_9847130
let vteumj = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns:svgjs='http://svgjs.com/svgjs' width='512' height='512' x='0' y='0' viewBox='0 0 24 24' style='enable-background:new 0 0 512 512' xml:space='preserve' class=''><g><g data-name='Layer 2'><rect width='5' height='20' x='4.5' y='2' rx='2.5' fill='black' data-original='black' class=''></rect><rect width='5' height='20' x='14.5' y='2' rx='2.5' fill='black' data-original='black' class=''></rect></g></g></svg>";

document.documentElement.style.setProperty("--Audio-Post-Pause-Btn-SVG",`url("${vteumj}")`);

// vroom vroom
const getSpeed = (s) => {
  let res;
  let nums = Number(s.replace(/[^\d\.]*/g,""));
  let units = s.toLowerCase().replace(/[^a-z]/g,"");
  units == "s" ? res = nums*1000 : res = nums;
  return res
}


/*--------- MAIN SHIT ---------*/

// audio volume
let audVol = getRoot("--Audio-Post-Volume");
audVol = audVol.trim() == "" ? 1 : audVol.indexOf("%") > -1 ? parseFloat(audVol) / 100 : 1;

/*------- REMOVE HREF.LI LINK REFERRER -------*/
/*
  shoutout: based on / inspired by @magnusthemes:
  v1: magnusthemes.tumblr.com/post/162657076440
  v2: magnusthemes.tumblr.com/post/643460974823833600
*/
const noHrefLi = () => {
  document.querySelectorAll("a[href*='href.li/?']")?.forEach(a => {
    let href = a.href;
    if(href.indexOf("href.li/?") > -1){
      a.href = href.replace("href.li/?","")
    }
  })

  // EXAMPLE: https://t.umblr.com/redirect?z=https%3A%2F%2Fgithub.com%2Feramdam%2FGifsOnTumblr&t=MDU4MDAzOTZkMmM5NmIzNmNjNzJmM2NlNzRkZjY1NWRlYTQyNGM3NiwwZmQwNjM0ODY0ZDBkY2ZlNWVmMjBhNjgyNTJlMDkzNTU2MTUwZTEy
  // SEEN IN THIS POST: felicitysmoak.tumblr.com/post/188159105501
  document.querySelectorAll("a[href*='t.umblr.com/redirect?z=']")?.forEach(a => {
    let href = a.href;
    if(href.indexOf("t.umblr.com/redirect?z=") > -1){
      let de = decodeURIComponent(href);
      a.href = de.replace("https://t.umblr.com/redirect?z=","").split("&t=")[0]
    }
  })
}

/*------- REMOVE EMPTY ELEMENTS -------*/
const removeEmptyStuff = () => {
  document.querySelectorAll(".comment-body p")?.forEach(p => {
    if(p.firstElementChild){
      if(p.querySelector("br:only-child")){
        p.querySelector("br:only-child").remove();
        if(p.innerHTML.trim() == ""){
          p.remove()
        }
      }
    }
  })

  setTimeout(() => {
    let stuff = ".comment-body > p:first-child, .comment-body:not(.add-op-comment .comment-body), .comment, .comments, .text-block, .a-text > p";

    document.querySelectorAll(stuff)?.forEach(el => {
      if(el.innerHTML.trim() == ""){
        el.remove()
      }
    })
    
    document.querySelectorAll(".comment.extra-comment:first-child:not(.add-op-comment) + .add-op-comment")?.forEach(c => {
      c.previousElementSibling.remove()
    })
    
    document.querySelectorAll(".original-comment + .add-op-comment, .add-op-comment:last-child:not(:first-child)")?.forEach(c => {
      c.remove()
    })
    
    document.querySelectorAll(".comment-body p")?.forEach(p => {
      if(p.outerHTML.trim() == "<p></p>"){
        p.remove()
      }
    })
    
    document.querySelectorAll(".a-text > p:first-child, .a-text > p:last-child")?.forEach(p => {
      if(p.outerHTML == "<p><br></p>" || p.outerHTML == "<p></p>"){
        p.remove();
      }
    })
  },0)
}

/*------- MOVE FIRST MEDIA INSIDE BLOCKQUOTE Y/N -------*/
const moveFirstMedia = () => {
  let move = getRoot("--Prepend-1st-Media");
  
  // ALWAYS move the post title REGARDLESS of user's prepend option
  // ignore the "not found" page title bc there is no .comment selector
  // example: file.garden/ZRt8jW_MomIqlrfn/screenshots/xqvvt.png
  document.querySelectorAll(".posts:not([id='post-']) .post-body > .text-block > h1.post-title:first-child")?.forEach(title => {
    let comments = title.nextElementSibling;
      if(comments && comments.matches(".comments")){
        let firstComment = comments.querySelector(".original-comment");
        
        // no caption, show OP head
        if(!firstComment){
          title.closest("[class*='-block']").classList.add("inc-op");
          let postBody = title.closest(".post-body");
          if(postBody){
            let body = postBody.querySelector(".add-op-comment .comment-body");
            if(body){
              body.prepend(title)
            }
          }
        }
        
        // has caption
        else {
          let body = firstComment.querySelector(".comment-body");
          if(body){
            body.prepend(title)
          }
        }
      }
  })
  
  setTimeout(() => {
    document.querySelectorAll(".posts.ex-npf[post-type='chat']")?.forEach(x => {
      let title = x.querySelector(".text-block > h1.post-title:first-child");
      if(title){
        let comment = x.querySelector(".comment:first-child .comment-body");
        if(comment){
          comment.prepend(title)
        }
      }
    })
  },1)
  
  /*----- OPTION: DON'T PREPEND FIRST MEDIA -----*/
  // first media is inside the blockquote
  if(move == "no"){
    document.querySelectorAll(".post-body > [class*='-block']:first-child > [class^='legacy-']:first-child")?.forEach(media => {
      let comments = media.nextElementSibling;
      if(comments && comments.matches(".comments")){
        let firstComment = comments.querySelector(".original-comment");
        
        // no caption, show OP head
        if(!firstComment){
          media.closest("[class*='-block']").classList.add("inc-op");
          let postBody = media.closest(".post-body");
          if(postBody){
            let body = postBody.querySelector(".add-op-comment .comment-body");
            if(body){
              body.prepend(media)
            }
          }
        }
        
        // has caption
        else {
          let body = firstComment.querySelector(".comment-body");
          if(body){
            body.prepend(media)
          }
        }
      }
    })
  }
  
  /*----- OPTION: PREPEND FIRST MEDIA -----*/
  // first media is outside the caption
  else {
    document.querySelectorAll(".add-op-comment")?.forEach(c => {
      if(!c.closest("[post-type='quote']:not(.ex-npf)")){
        c.remove();
      }
    })
  }
}

/*------- CHANGE .gifv TO .gif -------*/
const removeGIFv = () => {
  document.querySelectorAll("img[src$='.gifv']")?.forEach(img => {
    img.src = img.src.slice(0,-1);
  })
}

/*------- WRAP STRAY TEXT NODES -------*/
const commentBodyNodes = () => {
  document.querySelectorAll(".comment-body")?.forEach(el => {
    let stack = [el];

    while(stack.length > 0){
      // get latest child from stack
      let currentNode = stack.pop();
      
      // wrapping conditions:
      // 1. currentNode does NOT have child nodes (it IS the final descendant)
      // 2. currentNode is a text node
      // 3. currentNode is not empty
      // 4. if currentNode has a parent, the parent is NOT a <span>
      //    (prevents span > span > span > span buildup)
      if(currentNode.nodeType === 3 && currentNode.data.trim().length > 0 && !currentNode.parentNode?.matches("span")){
        let span = document.createElement("span");
        currentNode.before(span);
        span.appendChild(currentNode);
      } else if(currentNode.childNodes.length > 0){
        // currentNode HAS child nodes, add them to the stack
        for(let i=currentNode.childNodes.length-1; i>=0; i--){
          stack.push(currentNode.childNodes[i]);
        }
      }
    }
  })
}

/*------- LEGACY PHOTO(SET)S -------*/
function legacyIMG(w, h, ld, hd){
  this.width = Number(w);
  this.height = Number(h);
  this.low_res = ld;
  this.high_res = hd;
}

const legacyPhotos = () => {
  let old_photoset = document.querySelectorAll(".legacy-photoset[layout]:not([layout=''], .done)");
  old_photoset?.forEach((sett) => {
    let sett_ly = sett.getAttribute("layout");
    let sett_ar = [];
    let sett_xy = sett_ly.split("");

    sett_xy?.forEach(zett => {
      sett_ar.push(zett)
    })

    sett_ar.forEach((xett, i) => {
      let pylnn = document.createElement("div");
      pylnn.classList.add("layout-row");
      pylnn.setAttribute("cols", xett);
      pylnn.style.gridTemplateColumns = `repeat(${xett}, 1fr)`
      sett.append(pylnn);
    });

    sett.querySelectorAll(".layout-row:empty").forEach((lr, i) => {
      let curNum = parseInt(lr.getAttribute("cols"));
      sett.querySelectorAll("img").forEach((boo, i) => {
        i += 1;
        if(i <= curNum){
          lr.append(boo);
        }

        loadImage(boo.src).then(i => {
          let h = boo.offsetHeight;
          boo.setAttribute("offset-height",h)
        }).catch(err => console.error(err));
      });
    });
    
    sett.classList.add("done");

  }); // end .legacy-photoset forEach
  
  // legacy photoset row heights
  document.querySelectorAll(".layout-row[cols]")?.forEach(row => {
    let a = Date.now();
    let b = 6000;
    let c = setInterval(() => {
      if(Date.now - a > b){
        clearInterval(c)
      } else {
        if(!row.querySelector("img:not([offset-height]:not([offset-height='']))")){
          clearInterval(c);
          let cols = Array.from(row.querySelectorAll(":scope > img[offset-height]:not([offset-height=''])"));
          let minHeight = Math.min(...cols.map(col => Number(col.getAttribute("offset-height"))));
          row.style.height = `${minHeight}px`;
          row.querySelectorAll(":scope > img[offset-height]:not([offset-height=''])")?.forEach(i => {
            i.style.marginTop = `calc(0px - (${i.getAttribute("offset-height")}px - ${minHeight}px) / 2)`
          })
          row.classList.add("done");
          legacyPhotosHeights()
        }
      }
    })
  })
  
  // legacy lightboxes
  document.querySelectorAll(".legacy-photo, .legacy-photoset")?.forEach(sel => {
    let mbusv = [];
    sel.querySelectorAll("img:not(a[onclick*='Tumblr.PanoLightboxInit'] > img)").forEach((imgz, index) => {
      mbusv.push(new legacyIMG(
        imgz.getAttribute("width"),
        imgz.getAttribute("height"),
        imgz.src,
        imgz.src
      ));

      let bipuq = Math.floor(parseInt(index)+1);
      imgz.setAttribute("index",bipuq);
      sel.setAttribute("onclick",`Tumblr.Lightbox.init(${JSON.stringify(mbusv)},1)`);
      imgz.addEventListener("click", () => {
        sel.setAttribute("onclick",`Tumblr.Lightbox.init(${JSON.stringify(mbusv)},${bipuq})`);
      })
    })
  })
  
  // move legacy photo blocks
};

// call this on resize only
const legacyPhotosHeights = () => {
  document.querySelectorAll(".photo-block .layout-row[cols]")?.forEach(row => {
    let cols = Array.from(row.querySelectorAll(":scope > img"));
    let minHeight = Math.min(...cols.map(col => col.offsetHeight));
    row.style.height = `${minHeight}px`;
    row.querySelectorAll(":scope > img")?.forEach(i => {
      i.style.marginTop = `calc(0px - (${i.offsetHeight}px - ${minHeight}px) / 2)`
    })
  })
}

/*------- LEGACY VIDEOS -------*/
const legacyVideos = () => {
  document.querySelectorAll(".legacy-video .tumblr_video_container + .poster-thumb[url]:not([url=''])")?.forEach(poster => {
    let posterURL = poster.getAttribute("url");
    if(posterURL.trim() !== ""){
      let segment = posterURL.trim().split(".media.tumblr.com/tumblr_")[1].split("_")[0];

      let vidURL = `https://va.media.tumblr.com/tumblr_${segment}.mp4`
      fetch(vidURL).then(q => {
        if(!q.ok){
          // url does not exist
        }
        return q;
      }).then(c => {
        let newVid = document.createElement("video");
        newVid.src = vidURL;
        newVid.setAttribute("poster",posterURL);
        poster.after(newVid);

        if(typeof VIDYO === "function"){
          VIDYO("video")
        }

        poster.previousElementSibling.remove();
        poster.remove();
      }).catch(err => console.error(err))
    }
  })
}

/*------- QUOTE STUFF -------*/
const quoteStuff = () => {
  document.querySelectorAll("[post-type='text'] .text-block .comment:first-of-type .comment-body > blockquote:first-child + p:last-child")?.forEach(peas => {
    let bbq = peas.previousElementSibling;
    let bbqFirstChild = bbq.querySelector(":scope > *:first-child");
    if(bbqFirstChild){
      if(bbqFirstChild.matches("div:only-child")){
        // note: bbq is the <blockquote>
        bbq.classList.add("npf_quote");
        bbq.classList.add("quote-text");

        if(peas.innerHTML.trim() !== ""){
          peas.classList.add("quote-source")
        }

        let reblogContent = peas.closest(".comment-body");
        if(reblogContent){
          let quoteSet = document.createElement("div");
          quoteSet.classList.add("quote-set");
          reblogContent.prepend(quoteSet);
          reblogContent.querySelectorAll(":scope > *:not(.quote-set)")?.forEach(v => {
            quoteSet.append(v)
          })
          
          let comment = peas.closest(".comment");
          if(comment.matches(".extra-comment")){
            let opName = peas.closest("[post-type]").getAttribute("username");
            let opURL = peas.closest("[post-type]").getAttribute("root-url");
            
            let aSel = comment.querySelector(".comment-header > a");
            let imgSel = comment.querySelector(".comment-header img.userpic");
            let nameSel = comment.querySelector(".comment-header .username");
            
            aSel ? aSel.href = opURL : "";
            imgSel ? imgSel.src = `//api.tumblr.com/v2/blog/${opName}/avatar` : "";
            nameSel ? nameSel.textContent = opName : "";
            
            comment.setAttribute("username",opName);
            setTimeout(() => {
              commentMenu()
            },0)
          }
        }        
      }
    }
  })//end peas
}

/*------- CHAT STUFF -------*/
const chatStuff = () => {
  document.querySelectorAll(`.npf_chat, [data-npf*='{"subtype":"chat"}']`)?.forEach((chat) => {
    let contents = chat.childNodes;
    if(contents){
      let textNodes = Array.from(contents).filter((node) => {
        return node.nodeType === 3 && node.data.trim().length > 0;
      });

      textNodes.forEach((node) => {
        let wrapper = document.createElement("span");
        node.parentNode.insertBefore(wrapper, node);
        wrapper.appendChild(node);
      });
    }

    /*------- CHAT LABEL -------*/
    // YES chat label
    if(chat.querySelector("b")){
      let checkB = chat.querySelectorAll(":scope > b");
      checkB?.forEach(b => {
        if(!b.matches(".chat-label")){
          b.classList.add("chat-label");
        }
      })
    }

    // NO chat label? force it
    else {
      let makeB = document.createElement("b");
      makeB.classList.add("chat-label");
      chat.prepend(makeB)
    }

    /*------- CHAT CONTENT (sentence/line) -------*/
    chat.querySelectorAll(":scope > b.chat-label")?.forEach((label, i) => {
      // .next() is <span>
      let lnext = label.nextElementSibling;
      if(lnext && lnext.matches("span")){
        lnext.classList.add("chat-content");
      }
      // no .next()
      else {
        let sp = document.createElement("span");
        sp.classList.add("chat-content");
        label.after(sp);
      }          
    })

    let chatLabels = chat.querySelectorAll(".chat-label");
    let chatContents = chat.querySelectorAll(".chat-content");

    // loop through each .chat-label and create new p.npf_chat accordingly
    for(let i=0; i<chatLabels.length; i++){
      let newP = document.createElement("p");
      newP.classList.add("npf_chat")
      newP.classList.add("new_chat")

      let newB = document.createElement("b");
      newB.classList.add("chat-label");
      newB.textContent = chatLabels[i].textContent;

      let newSpan = document.createElement("span");
      newSpan.classList.add("chat-content");
      newSpan.textContent = chatContents[i].textContent;

      newP.appendChild(newB);
      newP.appendChild(newSpan);

      chat.before(newP)
    }

    chat.parentNode.removeChild(chat);
  })//end chat forEach


  document.querySelectorAll(`.npf_chat, [data-npf*='{"subtype":"chat"}']`)?.forEach((chat) => {
    // if .chat-content starts with ":"
    // & .chat-label does NOT end with ":"
    // do smth about it
    chat.querySelectorAll(".chat-label + .chat-content")?.forEach(content => {
      let labelText = content.previousElementSibling.textContent.trim();
      let contentText = content.textContent.trim();
      if(contentText.slice(0,1) == ":"){
        if(labelText.slice(-1) !== ":"){
          content.previousElementSibling.append(":");
          content.textContent = content.textContent.slice(1)
        }
      }
    })

    // change into <li>
    let li = document.createElement("li");
    li.classList.add("chat-line");
    chat.before(li);
    li.append(chat);
    chat.replaceWith(...chat.childNodes);
  });

  // wrap all neighboring .chatline[s] into .chatwrap
  document.querySelectorAll("[post-type='text'] *:not(.chat-wrap) li.chat-line")?.forEach(line => {
    let prevEl = line.previousElementSibling;
    if(!prevEl?.matches(".chat-line")){
      let cont = document.createElement("ul");
      cont.classList.add("chat-wrap");
      line.before(cont);

      let apres = cont.nextElementSibling;
      while(apres && apres.matches(".chat-line")){
        cont.appendChild(apres);
        apres = cont.nextElementSibling;
      }
    }
  })
  
  // deal with reblog heads that aren't the actual reblogroot
  setTimeout(() => {
    document.querySelectorAll("[post-type='chat'][username][root-url] .chat-block.inc-op .comments > .extra-comment:first-child, [post-type='chat'][username][root-url] .text-block .comments > .extra-comment:first-child")?.forEach(imposterComment => {
      let actualName = imposterComment.closest("[post-type][username]:not([username=''])");
      if(actualName){
        actualName = actualName.getAttribute("username");

        imposterComment.setAttribute("username",actualName);

        let userpic = imposterComment.querySelector(".comment-header img.userpic[alt]:not([alt=''])");
        if(userpic){
          let alt = userpic.alt;
          if(alt.indexOf("'s") > -1){          
            let trimApres = alt.split("'s")[1];
            userpic.alt = `${actualName}'s${trimApres}'`
          }
          
          if(actualName.indexOf("-deac") > -1){
            if(actualName.substring(actualName.lastIndexOf("-")+1).slice(0,4) == "deac"){
              
            } else {
              userpic.src = `//api.tumblr.com/v2/blog/${actualName}/avatar`
            }
          } else {
            userpic.src = `//api.tumblr.com/v2/blog/${actualName}/avatar`
          }
          
        }

        let usernameSpan = imposterComment.querySelector(".comment-header .username");
        if(usernameSpan){
          usernameSpan.textContent = actualName
        }
        
        let rootURL = imposterComment.closest("[post-type][root-url]:not([root-url=''])");
        if(rootURL){
          rootURL = rootURL.getAttribute("root-url");
          let perma = imposterComment.querySelector(".comment-header > a");
          if(perma){
            perma.href = rootURL
          }
        }
        
        setTimeout(() => {
          deactUsers();
          commentMenu()
        },0)
      }
    })
  },1)
}

/*------- NPF PHOTOS: MISC / OTHER -------*/
const npfIMGsMisc = () => {
  setTimeout(() => {
    document.querySelectorAll(".npf_group .npf_inst.npf_photo:first-child")?.forEach(i => {
      let parent = i.parentNode;
      if(!parent.querySelector(":scope > .npf_inst:not(.npf_photo)")){
        i.closest(".npf_group").classList.add("npf-photo-only")
      }
    })
  },0)
}

/*------- NPF PHOTOS: 1 COLUMN ONLY -------*/
const npf1Cols = () => {
  setTimeout(() => {
    document.querySelectorAll(".npf_photo .npf_row[columns='1'] [data-big-photo-width]:not([data-big-photo-width=''])")?.forEach(npf => {
      let w = Number(npf.getAttribute("data-big-photo-width"));
      if(w < Number(getRoot("--Post-Width").replace(/[^\d\.]*/g,""))){
        npf.closest(".npf_row[columns='1']").classList.add("smol")
      }
    })
    
    document.querySelectorAll(".npf_photo")?.forEach(npf_photo => {
      if(npf_photo.querySelector(".npf_row[columns='1']")){
        let allRowsInThis = npf_photo.querySelectorAll(":scope > .npf_row").length;
        let oneRows = [];
        npf_photo.querySelectorAll(":scope > .npf_row")?.forEach(row => {
          if(row.matches("[columns='1']")){
            oneRows.push("h");
          }
        })
        
        if(oneRows.length == allRowsInThis){
          npf_photo.classList.add("npf_single_each")
        }
      }
    })
    
    // see if .smol imgs exist in .npf_single_each[s]
    document.querySelectorAll(".npf_photo.npf_single_each")?.forEach(groupOfSolos => {
      if(groupOfSolos.querySelector(".smol")){
        groupOfSolos.classList.add("has-smol");
      }
    })
    
  },0)
}

/*------- LEGACY LINK STUFF -------*/
const legacyLinkStuff = () => {
  document.querySelectorAll("[post-type='link'] .link-block.inc-op .original-comment.not-reblog")?.forEach(ogComment => {
    let actualName = ogComment.closest("html[username]:not([username=''])");
    if(actualName){
      actualName = actualName.getAttribute("username");
      
      ogComment.closest("[post-type]").setAttribute("username",actualName)
      
      ogComment.setAttribute("username",actualName);
      
      let userpic = ogComment.querySelector(".comment-header img.userpic[alt]:not([alt=''])");
      if(userpic){
        let alt = userpic.alt;
        if(alt.indexOf("'s") > -1){          
          let trimApres = alt.split("'s")[1];
          userpic.alt = `${actualName}'s${trimApres}'`
        }
      }
      
      let usernameSpan = ogComment.querySelector(".comment-header .username");
      if(usernameSpan){
        usernameSpan.textContent = actualName
      }
      
      let srcpart = ogComment.closest("[post-type]").querySelector(".src-part");
      if(srcpart){
        let src_a = srcpart.querySelector("a");
        src_a ? src_a.textContent = actualName : ""
      }
      
      setTimeout(() => commentMenu(), 0);
    }
  })
}

/*------- NPF LINK STUFF -------*/
const npfLinkStuff = () => {
  document.querySelectorAll("[post-type='text'] .post-body .comments .comment-body .npf-link-block")?.forEach(linkBlock => {
    let titleText = "";
    let descText = "";
    let imgURL = "";
    let linkTarget = "";
    let linkHref = "";
    let siteName = "";

    let titleDiv = linkBlock.querySelector(".title");
    titleDiv ? titleText = titleDiv.textContent : null;

    let descDiv = linkBlock.querySelector(".description");
    descDiv ? descText = descDiv.textContent : null;

    let siteDiv = linkBlock.querySelector(".site-name");
    siteDiv ? siteName = siteDiv.textContent : null;

    if(linkBlock.matches(".has-poster")){
      let imgDiv = linkBlock.querySelector(".poster[style*='background-image:url(']");
      imgURL = imgDiv.style.backgroundImage.replace(/(^url\()|(\)$|[\"\'])/g,"");
    }

    let get_a = linkBlock.querySelector(":scope > a");
    if(get_a.matches("[target]")){
      linkTarget = get_a.getAttribute("target");
    }
    if(get_a.matches("[href]")){
      linkHref = get_a.getAttribute("href");
    }

    let makeA = document.createElement("a");
    makeA.classList.add("link-render");
    makeA.target = linkTarget;
    makeA.href = linkHref;

    let h2 = document.createElement("h2");
    h2.textContent = titleText.trim();

    let arrow = document.createElement("i");
    arrow.classList.add("eva-icons");
    arrow.setAttribute("icon-name","arrowhead-right-outline")

    let div = document.createElement("div");
    div.classList.add("site-name");

    let i = document.createElement("i")
    i.classList.add("eva-icons");
    i.setAttribute("icon-name","link-2-outline");

    let span = document.createElement("span");
    span.textContent = siteName.trim();

    linkBlock.before(makeA);
    makeA.append(h2)
    h2.append(arrow);
    makeA.append(div);
    div.append(i);
    div.append(span);

    linkBlock.remove();

  })//end npf link forEach

  document.querySelectorAll(".link-block a.link-render > h2:first-of-type")?.forEach(h2 => {
    h2.innerHTML = h2.innerHTML.trim();
  })
}

/*------- NPF VIDEO STUFF -------*/
const npfVideoStuff = () => {
  document.querySelectorAll(".tmblr-full video[autoplay]")?.forEach(v => {
    v.autoplay = false;
    v.muted = false;
  })
}

/*------- ASK POST STUFF -------*/
const askStuff = () => {
  // left or right alignment
  let ansAlign = getRoot("--Ask-Post-Answer-Alignment");
  if(ansAlign == "right"){
    document.documentElement.setAttribute("answer-right","");
  } else {
    document.documentElement.setAttribute("answer-left","");
  }
  
  document.querySelectorAll(".answer-block .q-top")?.forEach(q => {
    if(q.textContent.trim() !== "" && q.textContent.trim().indexOf(" ") > -1){
      let qtext = q.textContent.trim();
      let askerName = qtext.split(" ")[0];
      let img = q.closest(".answer-block").querySelector(".askerpic");
      img ? img.alt = `${askerName}'s avatar'` : null
    }
  })
  
  setTimeout(() => {
    document.querySelectorAll(".answer-block .answer-part.rb-answer .a-top")?.forEach(a => {
      if(a.textContent.trim() !== "" && a.textContent.trim().indexOf(" ") > -1){
        let atext = a.textContent.trim();
        let replyname = atext.split(" ")[0];
        let img = a.closest(".answer-block").querySelector(".replypic");
        img ? img.alt = `${replyname}'s avatar'` : null
      }
    })
  },0)
  
}

/*------- READ MORE STUFF -------*/
const readMoreStuff = () => {
  document.querySelectorAll(".comment-body p > a")?.forEach(a => {
    if(a.matches("a.read_more") || a.textContent.trim() == "Keep reading"){
      a.closest("p").classList.add("keep-reading")
    }
  })

  document.querySelectorAll(".posts .comments .comment.original-comment p.keep-reading")?.forEach(r => {
    let rootURL = r.closest(".posts[root-url][username]");
    if(rootURL){
      let username = rootURL.getAttribute("username").trim();
      rootURL = rootURL.getAttribute("root-url").trim();

      let readMoreLink = r.querySelector(":scope > a[href]");
      if(readMoreLink){
        if(readMoreLink.getAttribute("href") !== rootURL){
          readMoreLink.href = rootURL;

          let head = r.closest(".original-comment").querySelector(".comment-header[href]");
          if(head){
            if(head.getAttribute("href") !== rootURL){
              head.href = rootURL;

              head.querySelector(".username").textContent = username;                

              fetch(`https://api.tumblr.com/v2/blog/${username}/avatar`, {
                method: "GET",
                mode: 'no-cors'
              })
              .then(r => {
                let contentType = r.headers.get("content-type");
                if(contentType && contentType.indexOf("application/json") !== -1){
                  return r.json().then(data => {
                    // is "not found"
                  });
                } else {
                  return r.text().then(text => {
                    head.querySelector("img.userpic").src = `https://api.tumblr.com/v2/blog/${username}/avatar`
                  });
                }
              }).catch(err => console.error(err));
            }
          }
        }
      }
    }
  })
}

/*------- CAPS / UPPERCASE STUFF -------*/
const capsStuff = () => {
  let exceptions = `
    h1, h1 span,
    h2, h2 span,
    h3, h3 span,
    h4, h4 span,
    h5, h5 span,
    h6, h6 span,
    big
  `
  
  document.querySelectorAll(".comment-body, .aud-info, .status-text, .desc-cont")?.forEach(c => {
    // caps detection for status text
    if(c.matches(".status-text")){
      let t = c.textContent;
      if(t.trim() !== ""){
        t = t.trim();
        if(/^[A-Z\s\-@:]+$/.test(t.trim())){
          c.classList.add("caps")
        }
      }
    }
    
    // caps detection for everything else
    c.querySelectorAll(`*:not(${exceptions})`)?.forEach(s => {      
      if(!s.firstElementChild){        
        let t = s.textContent;
        if(t.trim() !== ""){
          if(/^[A-Z\s\-@:]+$/.test(t.trim())){
            s.classList.add("caps")
          }
        }
      }
    })
  })
}

/*------- LEGACY AUDIO -------*/
const legacyAudio = () => {
  document.querySelectorAll(".aud-gen .aud-info + .aud-iframe iframe.tumblr_audio_player")?.forEach((aud_embed) => {
    let audPostID = aud_embed.closest("[post-type='audio'][id]").getAttribute("id").replaceAll("post-","");

    let audioIMGArea = aud_embed.closest(".aud-gen")?.querySelector(".aud-cover img");
    let audioPlay = aud_embed.closest(".aud-gen")?.querySelector(".q-play");
    let audioPause = aud_embed.closest(".aud-gen")?.querySelector(".q-pause");
    let audioTextArea = aud_embed.closest(".aud-iframe")?.previousElementSibling;
    let audioDLArea = aud_embed.closest(".aud-gen")?.querySelector(".aud-dl");

    aud_embed.addEventListener("load", () => {
      let contents = aud_embed.contentDocument;

      let audSrc, audTitle, audArtist, audAlbumIMG, audAlbumName;

      // IS NULL
      if(contents === null){
        let useSrc = aud_embed.src;
        let trem = useSrc.split("?audio_file=")[1].split("&")[0];
        audSrc = decodeURIComponent(trem)
      }

      // IS NOT NULL
      else {
        audSrc = contents.querySelector("[data-stream-url]")?.getAttribute("data-stream-url");
        audTitle = contents.querySelector("[data-track]")?.getAttribute("data-track");
        audArtist = contents.querySelector("[data-artist]")?.getAttribute("data-artist");
        audAlbumIMG = contents.querySelector("[data-album-art]")?.getAttribute("data-album-art");
        audAlbumName = contents.querySelector("[data-album]")?.getAttribute("data-album");
      }//end: is NOT null


      // assign image
      // if(audioIMGArea){
      //   audioIMGArea.src = audAlbumIMG;
      // }

      // bind audio url to download button
      audioDLArea.href = audSrc;

      // create an audio element
      let newAud = document.createElement("audio");
      newAud.src = audSrc;
      aud_embed.closest(".aud-gen").append(newAud);
      newAud.volume = audVol;

      audioPlay.addEventListener("click", () => {
        if(newAud.paused){
          newAud.play();
        }
      });

      audioPause.addEventListener("click", () => {
        if(!newAud.paused){
          newAud.pause();
        }
      });

      newAud.addEventListener("play", () => {
        audioPlay.style.display = "none";
        audioPause.style.display = "flex";
      });

      newAud.addEventListener("pause", () => {
        audioPause.style.display = "none";
        audioPlay.style.display = "flex";
      });

      newAud.addEventListener("ended", () => {
        audioPause.style.display = "none";
        audioPlay.style.display = "flex";
      });
    }); //end audio load
  }); // end audio each (legacy)
  
  
}

/*------- NPF AUDIO -------*/
const neueAudio = () => {
  document.querySelectorAll("figcaption.audio-caption")?.forEach((npfAudio) => {
    // check if there's anything preceding text/content
    let prev = npfAudio.previousElementSibling;

    // check if there's anything after it
    let next = npfAudio.nextElementSibling;

    if(next){
      if(next.innerHTML.trim() == ""){
        npfAudio.classList.add("no-next");
      }
    } else {
      npfAudio.classList.add("no-next");
    }

    if(prev){
      if(prev.innerHTML.trim() == ""){
        npfAudio.classList.add("no-prev");
      }
    } else {
      npfAudio.classList.add("no-prev");
    }

    npfAudio.classList.remove("no-prev");
    npfAudio.classList.remove("no-next");

    let npfAudioDiv = document.createElement("div");
    npfAudioDiv.classList.add("aud-gen");
    npfAudio.before(npfAudioDiv);

    npfAudio.querySelectorAll(":scope > *").forEach((h) => {
      npfAudioDiv.append(h);
    });

    npfAudio.remove();

    npfAudioDiv.querySelectorAll(".tmblr-audio-meta").forEach((m) => {
      m.classList.remove("tmblr-audio-meta");
    });

    let deets = document.createElement("div");
    deets.classList.add("aud-info");

    let oldDeets = npfAudioDiv.querySelector(".audio-details");
    oldDeets.before(deets);
    oldDeets.querySelectorAll(":scope > *").forEach((o) => {
      deets.append(o);
    });

    oldDeets.remove();

    let xyz = document.createElement("div");
    xyz.classList.add("aud-xyz");
    deets.before(xyz);
    xyz.append(deets);

    let npfAudioTitle = npfAudioDiv.querySelector(".title");
    let npfAudioArtist = npfAudioDiv.querySelector(".artist");
    let npfAudioAlbum = npfAudioDiv.querySelector(".album");

    if(npfAudioTitle){
      let titleText = npfAudioTitle.textContent;
      if(titleText.trim() == ""){
        titleText = "Untitled Track";
      }
      let titleDiv = document.createElement("div");
      titleDiv.classList.add("aud-title");
      titleDiv.textContent = titleText;
      npfAudioTitle.before(titleDiv);
      npfAudioTitle.remove();
    }

    if(npfAudioArtist){
      let artistText = npfAudioArtist.textContent;
      if(artistText.trim() == ""){
        artistText = "Unknown Artist";
      }
      let artistDiv = document.createElement("div");
      artistDiv.classList.add("aud-artist");
      artistDiv.textContent = artistText;
      npfAudioArtist.before(artistDiv);
      npfAudioArtist.remove();
    }

    if(npfAudioAlbum){
      let albumText = npfAudioAlbum.textContent;
      if(albumText.trim() == ""){
        albumText = "Unknown Album";
      }
      let albumDiv = document.createElement("div");
      albumDiv.classList.add("aud-album");
      albumDiv.textContent = albumText;
      npfAudioAlbum.before(albumDiv);
      npfAudioAlbum.remove();
    }

    let aftermath = xyz.nextElementSibling;
    if(aftermath){
      if(aftermath.matches("img.album-cover")){
        xyz.before(aftermath); // put <img class='album-cover'> BEFORE .aud-xyz
        aftermath.classList.remove("album-cover");
      } else {
        // if .aud-xyz.next() !== img.album-cover,
        // make it
        let makeTheCover = document.createElement("img");
        makeTheCover.src = "https://assets.tumblr.com/images/x.gif";
        xyz.before(makeTheCover);
      }
    }

    // if there is no .aud-xyz.next(),
    // make img.album-cover
    else {
      let makeTheCover = document.createElement("img");
      makeTheCover.src = "https://assets.tumblr.com/images/x.gif";
      xyz.before(makeTheCover);
    }

    let audCover = document.createElement("div");
    audCover.classList.add("aud-cover");

    if(aftermath?.matches("img.album-cover")){
      aftermath.before(audCover);
      audCover.append(aftermath);
    } else {
      xyz.before(audCover);
      audCover.append(audCover.closest(".aud-gen").querySelector("img"));
    }

    let audCtl = document.createElement("div");
    audCtl.classList.add("aud-ctl");
    audCover.prepend(audCtl);

    // play container
    let cplay = document.createElement("button");
    cplay.classList.add("q-play");
    cplay.ariaLabel = "Play";
    audCtl.append(cplay);

    // play icon
    let playIcon = document.createElement("span");
    playIcon.classList.add("play-icon");
    playIcon.ariaHidden = true;
    cplay.append(playIcon);

    // pause container
    let cpause = document.createElement("button");
    cpause.classList.add("q-pause");
    cpause.ariaLabel = "Pause";
    audCtl.append(cpause);

    // pause icon
    let pauseIcon = document.createElement("span");
    pauseIcon.classList.add("pause-icon");
    pauseIcon.ariaHidden = true;
    cpause.append(pauseIcon);

    // audio url
    let audSrc;
    let actualAud = npfAudioDiv.nextElementSibling; // was xyz.nextElementSibling
    if(actualAud){
      if(actualAud.matches("audio")){
        if(actualAud.matches("[src]")){
          audSrc = actualAud.getAttribute("src");
        } else if(actualAud.querySelector("source[src]")){
          audSrc = actualAud.querySelector("source[src]").getAttribute("src");
        }
      }
    }

    // make audio btn <a>
    let a = document.createElement("a");
    a.classList.add("aud-dl");
    a.href = audSrc;
    a.target = "_blank";
    a.ariaLabel = "Download";
    xyz.after(a);

    // make audio dl icon
    let ic = document.createElement("i");
    ic.classList.add("mgc_download_2_line");
    a.append(ic);

    // set the volume
    actualAud.volume = audVol;

    // play and pause events
    cplay.addEventListener("click", () => {
      if(actualAud.paused){
        actualAud.play();
      }
    });

    cpause.addEventListener("click", () => {
      if(!actualAud.paused){
        actualAud.pause();
      }
    });

    actualAud.addEventListener("play", () => {
      cplay.style.display = "none";
      cpause.style.display = "flex";
    });

    actualAud.addEventListener("pause", () => {
      cpause.style.display = "none";
      cplay.style.display = "flex";
    });

    actualAud.addEventListener("ended", () => {
      cpause.style.display = "none";
      cplay.style.display = "flex";

      // autoplay the next audio if there is one
      // npfAudioDiv == .aud-gen
      let figNext = npfAudioDiv.closest(".npf_audio").nextElementSibling; /* was npfAudioDiv.nextElementSibling */
      if(figNext && figNext.matches(".npf_audio")){
        figNext.querySelector(".q-play")?.click();
      }
    });
  }); //end npfAudio each

  setTimeout(() => {
    document.querySelectorAll(".npf_group .npf_inst.npf_audio:first-child")?.forEach(i => {
      let parent = i.parentNode;
      if(!parent.querySelector(":scope > .npf_inst:not(.npf_audio)")){
        i.closest(".npf_group").classList.add("npf-audio-only")
      }
    })
  },0)
}; //end neueAudio()

/*------- SOUNDCLOUD -------*/
const soundcloudStuff = () => {
  let player_btn_color = getRoot("--Audio-Post-Btns-BG");
  let soundcloud_height = Number(getRoot("--SoundCloud-Player-Height").replace(/[^\d\.]*/g,""));
  let scAlbumShowHide = getRoot("--SoundCloud-Show-Album-Image");
  soundcloud_height = !soundcloud_height ? 116 : soundcloud_height;
  scAlbumShowHide = scAlbumShowHide == "yes" ? "true" : "false"
  // minimalist soundcloud player: @shythemes
  // shythemes.tumblr.com/post/114792480648
  document.querySelectorAll("iframe[src*='soundcloud.com']:not(.done)")?.forEach((sc) => {
    let curSrc = sc.getAttribute("src").split("&")[0];
    sc.src = `${curSrc}&amp;liking=false&amp;sharing=false&amp;auto_play=false&amp;show_comments=false&amp;continuous_play=false&amp;buying=false&amp;show_playcount=false&amp;show_artwork=${scAlbumShowHide}&amp;origin=tumblr&amp;color=${player_btn_color.split("#")[1]}`;
    (sc.height = soundcloud_height), (sc.width = "100%");

    setTimeout(() => {
      soundcloud_height == 20 ? sc.closest("figure")?.classList.add("sc-short") : null;
      sc.classList.add("done");
    },0)
  });
}

/*------- STOP AUDIOS FROM PLAYING OVER E/O -------*/
// credit: stackoverflow.com/a/19792168/8144506
const audioNoConflict = () => {
  document.addEventListener("play", (e) => {
    let auds = document.querySelectorAll("audio");
    auds?.forEach((a,j) => {
      if(auds[j] != e.target){
        auds[j].pause();
        auds[j].currentTime = 0;
      }
    })
  },true)
}

/*------- REASSIGN POST TYPES -------*/
const redoPostTypes = () => {
  setTimeout(() => {
    document.querySelectorAll("[post-type='text']:not(.ex-npf) .post-body > *:first-child")?.forEach(target => {
      let post = target.closest("[post-type='text']");

      if(target.matches(".photo-origin")){
        // link
        if(target.matches(".link-render")){
          post.classList.add("ex-npf");
          post.setAttribute("post-type","link")
        }

        let innerFirst = target.firstElementChild;
        if(!innerFirst) return;

        // image
        if(innerFirst.matches(".npf_inst.npf_photo")){
          post.classList.add("ex-npf");
          post.setAttribute("post-type","photo")
        }

        // audio
        else if(innerFirst.matches(".npf_inst.npf_audio")){
          post.classList.add("ex-npf");
          post.setAttribute("post-type","audio")
        }

        // video
        else if(innerFirst.matches(".npf_inst.npf_video")){
          post.classList.add("ex-npf");
          post.setAttribute("post-type","video")
        }
      }//end: if: is .photo-origin
    })//end: post each

    document.querySelectorAll("[post-type='text']:not(.ex-npf) .post-body .comments .comment:first-child .comment-body > *:first-child")?.forEach(firstCommentBody => {
      let post = firstCommentBody.closest("[post-type='text']");

      // poll
      if(firstCommentBody.matches(".poll-post")){
        post.classList.add("ex-npf");
        post.setAttribute("post-type","poll")
      }

      // chat
      else if(firstCommentBody.matches(".chat-wrap")){
        post.classList.add("ex-npf");
        post.setAttribute("post-type","chat")
      }

      // quote
      else if(firstCommentBody.matches(".quote-set")){
        post.classList.add("ex-npf");
        post.setAttribute("post-type","quote")
      }
    })//end: post each
  },0)
}

/*------- CHANGE CURLY QUOTES TO STRAIGHT ONES -------*/
const uncurly = () => {
  document.querySelectorAll("pre, code, .npf_chat, .chat-content")?.forEach((code) => {
    let stuff = code.innerHTML;
    stuff = stuff.replace(/\u201C/g,'"').replace(/\u201D/g,'"')
    code.innerHTML = stuff;
  });
}

/*------- DEACTIVATED USERS -------*/
const deactUsers = () => {
  // reblog heads
  document.querySelectorAll(".comment[username]:not([username=''])")?.forEach(user => {
    let username = user.getAttribute("username");
    let usernameText = username.trim();
    
    if(usernameText.indexOf("-deac") > -1){
      let avantDash = username.substring(0,username.lastIndexOf("-"));

      let usernameDiv = user.querySelector(".username");
      let deacDiv = user.querySelector(".deactivated");

      usernameDiv ? usernameDiv.textContent = avantDash : null;
      if(!deacDiv){
        let d = document.createElement("span");
        d.classList.add("deactivated");
        d.textContent = "(deactivated)";
        usernameDiv.after(d);
      }
    }
  })//end reblog heads (comments)
  
  // answerer
  document.querySelectorAll(".answer-block .answer-part.rb-answer .a-top")?.forEach(answerWrap => {
    let a = answerWrap.querySelector("a[href]:not([href=''])");
    let text = a.textContent;
    if(text.indexOf("-deac") > -1){
      let apresDash = text.substring(text.lastIndexOf("-")+1);
      if(apresDash.slice(0,4) == "deac"){
        let name = text.substring(0,text.lastIndexOf("-"));
        a.textContent = name;
        
        let deacDiv = answerWrap.querySelector(".deactivated");
        if(!deacDiv){
          let d = document.createElement("span");
          d.classList.add("deactivated");
          d.textContent = " (deactivated)";
          a.after(d);
        }
      }
    }
  })//end answerer
  
  // via & src
  document.querySelectorAll(".via-part a, .src-part a")?.forEach(user => {
    let username = user.textContent;
    let usernameText = username.trim();
    
    if(usernameText.indexOf("-deac") > -1){
      let avantDash = username.substring(0,username.lastIndexOf("-"));

      user.textContent = `${avantDash} (deactivated)`
    }
  })//end via & src
}

/*------- COMMENT MENUS -------*/
const commentMenu = () => {
  document.querySelectorAll(".posts[post-url][reblog-url] .comment[username]:not([username='']) .comment-header .comment-dots")?.forEach(dots => {
    // current comment's username
    let username = dots.closest(".comment[username]").getAttribute("username");
    
    // current comment's permalink
    let commentLink = dots.closest(".comment-header").querySelector("a[href]:not([href=''])");
    
    // entire post's username
    let postPermalink = dots.closest(".posts[post-url]").getAttribute("post-url");
    
    let reblogURL = dots.closest(".posts[reblog-url]").getAttribute("reblog-url");
    
    let menu = dots.closest(".comment-header").querySelector(".comment-menu");
    
    if(menu){
      // if deactivated, set everything to the current blog's links
      if(dots.closest(".comment[username]").matches(".deactivated")){
        menu.querySelectorAll(":scope > a")?.forEach(a => {
          if(a.matches(".cm-view-blog")){
            a.href = `//${username}.tumblr.com`
          }
          
          if(a.matches(".cm-post-link")){
            a.href = postPermalink
          }
          
           else if(a.matches(".cm-reblog-link")){
            a.href = reblogURL
          }
        })
      }
      
      // not deactivated
      else {
        if(commentLink){
          let getCommentLink = commentLink.href;
          menu.querySelectorAll(":scope > a")?.forEach(a => {
            if(a.matches(".cm-view-blog")){
              a.href = `//${username}.tumblr.com`
            }

            if(a.matches(".cm-post-link")){
              a.href = getCommentLink
            }

            else if(a.matches(".cm-reblog-link")){
              if(getCommentLink.indexOf("/post/") > -1){
                let getPostID = getCommentLink.split("/post/")[1];
                if(getPostID.indexOf("/") > -1){
                  getPostID = getPostID.split("/")[0] // in case the caption text is part of the reblog fragment
                }
                let reblogFragment = reblogURL.substring(reblogURL.lastIndexOf("/")+1);
                a.href = `//tumblr.com/reblog/${username}/${getPostID}/${reblogFragment}`
              }
            }
          })
        }
        
      }//end else
      
      dots.addEventListener("click", (e) => {
        if(!menu.matches(".open")){
          document.querySelectorAll(".comment-menu")?.forEach(m => {
            if(m !== e.target){
              m.classList.remove("open")
            }
          })
          
          setTimeout(() => {
            menu.classList.add("open");
          },0)
          
        } else {
          setTimeout(() => {
            document.querySelectorAll(".comment-menu")?.forEach(m => {
              m.classList.remove("open")
            })
          },0)
          
        }
      })
      
    }//end: if menu exists
  })
}

/*------- OVERSCROLL Y/N -------*/
const overscroll = () => {
  // prevent trackpad overscroll ONLY on desktops
  if(/Mobi|Android/i.test(navigator.userAgent)){
    // on mobile
    document.querySelectorAll("html, body")?.forEach(s => {
      s.matches(".no-overscroll") ? s.classList.remove("no-overscroll") : null;
      s.matches(".device-desktop") ? s.classList.remove("device-desktop") : null;
      s.classList.add("device-touch");
    })
  }

  else {
    // on desktop
    document.querySelectorAll("html, body")?.forEach(s => {
      !s.matches(".no-overscroll") ? s.classList.add("no-overscroll") : null;      
      s.matches(".device-touch") ? s.classList.remove("device-touch") : null;
      s.classList.add("device-desktop")
    })
  }
}

/*------- TURN [title] TO [aria-label] -------*/
const ariaLabels = () => {
  document.querySelectorAll("[title]:not([title=''])")?.forEach(t => {
    let tt = t.title;
    t.removeAttribute("title");
    t.setAttribute("aria-label",tt);
  })
}

/*------- COPY LINK TO POST // COPY POST LINK -------*/
// clipboardJS by zenorocha
// github.com/zenorocha/clipboard.js
const copyPostLink = () => {
  document.querySelectorAll("[data-clipboard-text]:not([data-clipboard-text=''])")?.forEach(t => {
    let clipboard = new ClipboardJS(t);
    
    let copyZone = t.closest(".copy-area");
    if(copyZone){
      clipboard.on("success", () => {
        copyZone.classList = `c ${copyZone.classList.value}`
        setTimeout(() => {
          copyZone.classList.remove("c")
        },1000)
      })
    }
  })
}

/*------- TOGGLE TAGS -------*/
const togTags = () => {
  document.querySelectorAll(".posts[tags-vis]:not([tags-vis=''])")?.forEach(post => {
    let tagvis = post.getAttribute("tags-vis").trim();
    if(tagvis == "toggle"){
      let tagscont = post.querySelector(".tagscont");
      let tagsbtn = post.querySelector(".tags-icon-area button");
      if(tagscont && tagsbtn){
        tagsbtn.addEventListener("click", () => {
          // closed -> open
          if(!post.matches(".tags-clicked")){
            setTimeout(() => {
              post.classList.add("tags-clicked");
            },0)
          }
          
          // open -> closed
          else {
            // setTimeout(() => {
            //   post.classList.remove("tags-clicked")
            // },getSpeed(getRoot("--Tags-Fade-Speed")))
            
            setTimeout(() => {
              post.classList.remove("tags-clicked")
            },0)
          }
        })
      }
    }
  })
}

/*-------TIMEAGO -------*/
// originally by bychloethemes.tumblr.com/plugins/timeago
// reworked into vanilla JS by mournstera.tumblr.com/post/738426605346652160
const timeAgoStuff = () => {
  if(getRoot("--TimeAgo-Shorten") == "yes"){
    let jikan = document.querySelectorAll(".date-part .tsx");
    
    if(typeof timeAgo == "function"){
      timeAgo(jikan, {
          time: "short", // should be 'letter' 'short' or 'word'
          spaces: true, // 'true' adds spaces between words and numbers
          words: false, // 'true' turns numbers to words
          prefix: "",  // adds a prefix. could be '~' or 'about' or 'posted' etc.
          suffix: "ago" // adds a suffix. could be 'ago' or period, etc.
      })
      
      jikan?.forEach(x => {
        x.classList.add("tsz");
        x.classList.remove("tsx")
      })
    }
  }
}

/*------- TOOLTIPS -------*/
const tippys = (el) => {
  document.querySelectorAll(`[${el}]:not([${el}=''])`)?.forEach(x => {
    if(x.getAttribute(el).trim() !== ""){
      if(el === "title"){
        let ogTitle = x.getAttribute(el);
        x.addEventListener("mouseenter", () => {
          x.removeAttribute("title")
        })

        x.addEventListener("mouseleave", () => {
          x.setAttribute("title",ogTitle)
        })
      }
      tippy(x, {
        content: x.getAttribute(el),
        maxWidth: "var(--Post-Width)",
        followCursor: true,
        arrow: false,
        offset: [0, 18],
        moveTransition: "transform 0.015s ease-out",
      })
    }
  })
}

/*------- FADE IN STUFF AFTER INITIALIZING -------*/
const fadeLoad = () => {
  let vp = getSpeed(getRoot("--Load-In-Delay"));
  let sp = getSpeed(getRoot("--Load-In-Speed"));
  vp = vp === "" ? 0 : vp;
  sp = sp === "" ? 400 : sp;

  document.querySelectorAll(".load-in:not(.load-removing)")?.forEach(el => {
    setTimeout(() => {
      el.classList.remove("load-in");
      el.classList.add("load-removing");
      setTimeout(() => {
        el.classList.remove("load-removing");
        el.getAttribute("class").trim() == "" ? el.removeAttribute("class") : null
      },sp)
    },vp)
  })
}

/*------- BACK TO TOP BUTTON ------*/
const backToTopBtn = () => {
  let btt = document.querySelector(".back-to-top:not(.btt-hide) > button");
  if(btt){
    window.addEventListener("scroll", (e) => {
      let scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
      if(scrollTop >= 500){
        btt.parentNode.classList.add("f");
      } else {
        btt.parentNode.classList.remove("f");
      }
    })

    btt.addEventListener("click", () => {
      document.documentElement.scrollTop = 0;
    })
  }
}

/*------- SPINNER SVG ------*/
const spinzaku = () => {
  document.querySelectorAll(".spinner-svg")?.forEach(s => {
    s.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" width="512" height="512" x="0" y="0" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512" xml:space="preserve" class=""> <g> <path d="M256 150a25 25 0 0 1-25-25V25a25 25 0 0 1 50 0v100a25 25 0 0 1-25 25Z" fill="black"></path> <path d="M348.63 188.37A25 25 0 0 1 331 145.69L401.66 75A25 25 0 1 1 437 110.34l-70.71 70.71a24.93 24.93 0 0 1-17.66 7.32Z" fill="black"></path> <path d="M487 281H387a25 25 0 0 1 0-50h100a25 25 0 0 1 0 50Z" fill="black"></path> <path d="M419.34 444.34a24.94 24.94 0 0 1-17.68-7.34L331 366.31A25 25 0 0 1 366.31 331L437 401.66a25 25 0 0 1-17.68 42.68Z" fill="black"></path> <path d="M256 512a25 25 0 0 1-25-25V387a25 25 0 0 1 50 0v100a25 25 0 0 1-25 25Z" fill="black"></path> <path d="M92.66 444.34A25 25 0 0 1 75 401.66L145.69 331a25 25 0 0 1 35.36 35.36L110.34 437a24.94 24.94 0 0 1-17.68 7.34Z" fill="black"></path> <path d="M125 281H25a25 25 0 0 1 0-50h100a25 25 0 0 1 0 50Z" fill="black"></path> <path d="M163.37 188.37a24.93 24.93 0 0 1-17.68-7.32L75 110.34A25 25 0 1 1 110.34 75l70.71 70.71a25 25 0 0 1-17.68 42.68Z" fill="black"></path> </g> </svg>`
  })
}

/*------- SHORTEN NOTE COUNT ------*/
// credit/originally from shythemes
// shythemes.tumblr.com/post/156021137818
const shortenNotes = () => {
  if(getRoot("--Shorten-NoteCount") == "yes"){
    document.querySelectorAll(".notes-part")?.forEach(n => {
      let t = n.innerHTML.trim().split(" ")[0].replace(/,/g,"");
      if(!isNaN(Number(t))){
        if(t > 999){
          t = Math.floor(t / 100) / 10;
          n.innerHTML = `${t}k notes`
        }
      }
    })
  }
}

/*------- INFINITE SCROLL -------*/
const iscroll = () => {
  let postsCont = document.querySelector(".all-posts");
  let infScroll = new InfiniteScroll(postsCont, {
    path: ".botpagi .spinner a", // next page url selector
    append: ".posts", // posts selector
    history: false, // opts: infinite-scroll.com/options#history
    scrollThreshold: document.querySelector(".botpagi").offsetHeight,
    onInit: function(){
      this.on("scrollThreshold", () => {
        document.querySelectorAll(".bp-wrap.spinner")?.forEach(lol => {
          if(!lol.style.opacity == "1"){
            lol.style.opacity = 1;
          }
        })
      })
      
      this.on("load", () => {
        setTimeout(() => {
          typeof NPFv4 == "function" ? NPFv4() : ""
          setTimeout(() => themeInit(), 0);
          
          typeof glenFrames == "function" ? glenFrames() : ""
          
          copyPostLink();
          
          // WIGGLE
          setTimeout(() => {
            if(document.querySelector(".npf_photo")){
              let winEvt;
              if(typeof(Event) === "function"){
                winEvt = new Event("resize");
              } else {
                /*IE*/
                winEvt = document.createEvent("Event");
                winEvt.initEvent("resize", true, true);
              }
              window.dispatchEvent(winEvt);
            }
          },0)//end wiggle
        },0)
      })//end this.onload
      
      // like button status for each post with infscroll
      this.on("append", (body, path, items, response) => {
        let addedPostIDs = Array.from(items).map(e => {
          return e.getAttribute("id").split("post-")[1]
        })

        Tumblr.LikeButton.get_status_by_post_ids(addedPostIDs);
        
        fadeLoad();
      })
      
      // hide pagination / spinner when there's no next page to load
      this.on("last", (body, path) => {
        document.querySelectorAll(".botpagi")?.forEach(v => v.remove())
      })
    }//end onInit
  });//end infscroll
}//end iscroll func

/*------- THEME INIT -------*/
const themeInit = () => {
  document.querySelectorAll(".tumblr_preview_marker___")?.forEach(m => {
      m.remove();
  })
  
  // housekeeping stuff:
  noHrefLi();
  commentBodyNodes();
  
  // essential theme stuff:
  legacyPhotos();
  legacyVideos();
  quoteStuff();
  chatStuff();
  npfIMGsMisc();
  npf1Cols();
  legacyLinkStuff();
  npfLinkStuff();
  npfVideoStuff();
  askStuff();
  readMoreStuff();
  
  legacyAudio();
  neueAudio();
  soundcloudStuff();
  audioNoConflict();
  
  // extra theme stuff:
  moveFirstMedia();
  removeGIFv();
  capsStuff();
  shortenNotes();
  
  typeof VIDYO === "function" ? VIDYO("video") : null;
  
  redoPostTypes();
  removeEmptyStuff();
  uncurly();
  deactUsers();
  timeAgoStuff();
  
  commentMenu();
  
  copyPostLink();
  togTags();
  
  ariaLabels();
  tippys("aria-label");
}//end themeInit()

/*------- ON INITIAL LOAD -------*/
document.addEventListener("DOMContentLoaded", () => {
  let lsdkjfklds = document.querySelector("link[href*='unpkg.com/@phosphor-icons/web@latest/src/bold/style.css'][rel='stylesheet']");
  if(lsdkjfklds){
    let sfkljdskl = document.createElement("link");
    sfkljdskl.href = `https://unpkg.com/@phosphor-icons/web@2.0.1/src/bold/style.css`;
    sfkljdskl.rel = "stylesheet";
    document.head.append(sfkljdskl);
    lsdkjfklds.remove();
  }
  screenDims();
  custard();
  themeInit();
  overscroll();
  fadeLoad();
  backToTopBtn();
  spinzaku();
  
  // only run infinite scroll if:
  // 1. iscroll is enabled by user
  // 2. current page is index page, aka has posts
  // 3. if 'next' pagi selector exists (aka if a next page is available)
  if(document.querySelector("html").matches("[infscroll='yes'][page='index-page']") && document.querySelector(".botpagi .spinner a")){
    iscroll();
  }
})//end DOMContentLoaded

window.addEventListener("resize", () => {
  legacyPhotosHeights();
  screenDims();
})

/* note: the following CANNOT be 'const' */
function tumblrNotesInserted(){
  ariaLabels();
  tippys("aria-label");
  
  // making sure that "JohnSmith posted this" is always the last item otherwise it's weird lol
  document.querySelectorAll("ol.notes li.original_post")?.forEach(john => {
    if(john.nextElementSibling && john.nextElementSibling.matches("li:not(.more_notes_link_container)")){
      john.remove();
    }
  })
}