![Screenshot preview of the theme "Clotho" by glenthemes](https://static.tumblr.com/gtjt4bo/ntTsabeq2/clotho_1b.png)

**Theme no.:** 39  
**Theme name:** Clotho  
**Theme type:** Free / Tumblr use  
**Description:** A minimal, single-columned theme inspired by threads.net  
**Author:** @&hairsp;glenthemes  

**Release date:** 2023-03-14  
**Last updated:** 2023-03-14

**Post:** [glenthemes.tumblr.com/post/744991449083412480](https://glenthemes.tumblr.com/post/744991449083412480)  
**Preview:** [glenthpvs.tumblr.com/clotho](https://glenthpvs.tumblr.com/clotho)  
**Download:** [glen-themes.gitlab.io/thms/39/clotho](https://glen-themes.gitlab.io/thms/39/clotho)  
**Credits:** [glencredits.tumblr.com/clotho](https://glencredits.tumblr.com/clotho)